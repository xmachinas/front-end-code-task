const COLOURS = ['red', 'green', 'blue', 'yellow'];
const MAX_X = 10;
const MAX_Y = 10;

export class Block {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.colour = COLOURS[Math.floor(Math.random() * COLOURS.length)];
    this.clickable = true;
  }
}

export class BlockGrid {
  constructor() {
    this.grid = [];

    for (let x = 0; x < MAX_X; x++) {
      const col = [];
      for (let y = 0; y < MAX_Y; y++) {
        col.push(new Block(x, y));
      }

      this.grid.push(col);
    }
  }

  redrawBlock(blockEl, block) {
    const { x, y, colour } = block;

    blockEl.id = `block__${x}x${y}`;
    blockEl.className = `block block__${colour}`;
  }

  getNeighbours(block) {
    // TODO: How do we get all neighbours of a block given its coordinates
  }

  getSameColourNeighbours(block) {
    // TODO: How do we get only blocks of the same color?
  }

  render(grid = document.querySelector('#gridEl')) {
    const el = grid.cloneNode(false);
    grid.parentNode.replaceChild(el, grid);
    for (let x = 0; x < MAX_X; x++) {
      const id = 'col_' + x;
      const colEl = document.createElement('div');
      colEl.className = 'col';
      colEl.id = id;
      el.appendChild(colEl);

      for (let y = MAX_Y - 1; y >= 0; y--) {
        const block = this.grid[x][y];
        const blockEl = document.createElement('div');

        if (block.clickable) {
          blockEl.addEventListener('click', (evt) =>
            this.blockClicked(evt, block)
          );
        }

        colEl.appendChild(blockEl);
        this.redrawBlock(blockEl, block);
      }
    }

    return this;
  }

  blockClicked(e, block) {
    // TODO: What happens on each block click before we re-render?
    // TODO: What happens to each column of blocks?
    this.render();
  }
}
